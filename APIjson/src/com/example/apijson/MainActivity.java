package com.example.apijson;

import java.io.File;
import java.util.HashMap;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	private ListView list;
	private ListAdapter listAdapter;
	private String name;
	private String desc;
	private ProgressDialog progress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		list = (ListView) findViewById(R.id.listMain);
		
		Vector<HashMap<String, String>> vData = new Vector<HashMap<String, String>>();
		
		//ImageLoader config
		File cacheDir = StorageUtils.getCacheDirectory(this);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
        .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
        .discCacheExtraOptions(480, 800, CompressFormat.JPEG, 75, null)
        .threadPoolSize(3) // default
        .threadPriority(Thread.NORM_PRIORITY - 1) // default
        .tasksProcessingOrder(QueueProcessingType.FIFO) // default
        .denyCacheImageMultipleSizesInMemory()
        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
        .memoryCacheSize(2 * 1024 * 1024)
        .memoryCacheSizePercentage(13) // default
        .discCache(new UnlimitedDiscCache(cacheDir)) // default
        .discCacheSize(50 * 1024 * 1024)
        .discCacheFileCount(100)
        .discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
        .imageDownloader(new BaseImageDownloader(this)) // default
        .imageDecoder(new BaseImageDecoder()) // default
        .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
        .build();
		
		ImageLoader.getInstance().init(config);
		
		//Progress bar while loading JSON
		progress = ProgressDialog.show(this, "API", "Loading...");
		String url = "http://mstage.truelife.com/api_test/feed";
		
		
		
				//---- Lib Async Http --------------
				AsyncHttpClient client = new AsyncHttpClient();
				client.get(url, new AsyncHttpResponseHandler() {
					
				    @Override
				    public void onSuccess(final String arg0) {
				        super.onSuccess(arg0);
				        
				        final Vector<HashMap<String, String>> vData = new Vector<HashMap<String, String>>();
				        
				        //json
				        try {
							JSONObject json = new JSONObject(arg0);
							JSONArray jsonItem = json.getJSONArray("items");
							
							for(int i=0; i<jsonItem.length(); i++){
								
								JSONObject jsonData = jsonItem.getJSONObject(i);
								
								String title = jsonData.getString("title");
								String desc = jsonData.getString("desc");
								String image = jsonData.getString("image");
								
								HashMap<String, String> hData = new HashMap<String, String>();
								hData.put("title", title);
								hData.put("desc", desc);
								hData.put("image", image);
								
								vData.addElement(hData);
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				        
				        runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(progress.isShowing())
									progress.dismiss();
								 //txt.setText(arg0);
								
								
								listAdapter = new ListAdapter(vData);
								list.setAdapter(listAdapter);	
									
									
								
							}
							
				    		
				    		
				    	});
				        
				       
				    }
				    
				    @Override
				    public void onFailure(Throwable arg0, String arg1) {
				    	// TODO Auto-generated method stub
				    	super.onFailure(arg0, arg1);
				    	
				    	runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(progress.isShowing())
									progress.dismiss();
							}
				    		
				    		
				    	});
				    	
				    	
				    	
				    }
				});
		
		
	}
	
	class ListAdapter extends BaseAdapter{
		
		Vector<HashMap<String, String>> vData;
		
		ListAdapter(Vector<HashMap<String, String>> vData){
			this.vData = vData;
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return this.vData.size();
		}

		@Override
		public Object getItem(int index) {
			// TODO Auto-generated method stub
			return this.vData.elementAt(index);
		}

		@Override
		public long getItemId(int index) {
			// TODO Auto-generated method stub
			return index;
		}

		@Override
		public View getView(int index, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			HashMap<String, String> hData = vData.elementAt(index);
			
			View v;
			
			if(convertView == null){
				v = getLayoutInflater().inflate(R.layout.element, parent, false);
			}else{
				v = convertView;
			}
			
			TextView txtName = (TextView) v.findViewById(R.id.txtName);
			TextView txtDesc = (TextView) v.findViewById(R.id.txtDesc);
			

			ImageView img = (ImageView) v.findViewById(R.id.imgView);
			ImageLoader.getInstance().displayImage(hData.get("image"), img);
			
			
			txtName.setText(hData.get("title"));
			txtName.setTextSize(17);
			
			txtName.setTextColor(Color.WHITE);
			txtDesc.setTextColor(Color.WHITE);
			
			txtDesc.setText(hData.get("desc"));
			
			if(index % 2 == 0){
				v.setBackgroundColor(Color.parseColor("#43609C"));	
			}else{
				v.setBackgroundColor(Color.parseColor("#6B8DD6"));
			}
			
			
			return v;
		}
		
	}


}
